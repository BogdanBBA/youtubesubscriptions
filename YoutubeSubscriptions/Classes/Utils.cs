﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace YoutubeSubscriptions.Classes
{
	public static class Utils
	{
		private static string ColonReplacement = "coLonSsWag";

		public static TDatabase CreateInitializationDatabase()
		{
			TVideo vid = new TVideo();
			vid.ID = "id";
			vid.Uploaded = DateTime.Now;
			vid.Title = "title";
			vid.Duration = 120;
			vid.Views = 10000;
			vid.Likes = 100;
			vid.Dislikes = 20;
			vid.AvgRating = 4.3;
			vid.Comments = 17;
			TYoutuber yt = new TYoutuber();
			yt.ID = "id";
			yt.Name = "name";
			yt.InfoLastUpdated = DateTime.Now;
			yt.Joined = DateTime.Now.AddYears(-1);
			yt.Subscribers = 42000;
			yt.TotalVideos = 231;
			yt.TotalViews = 175000;
			yt.Videos = new List<TVideo>(new TVideo[1] { vid });
			TDatabase db = new TDatabase();
			db.Youtubers = new List<TYoutuber>(new TYoutuber[1] { yt });
			return db;
		}

		public static DateTime DecodeYoutubeApiDate(string encodedDate)
		{
			int y = 2000, M = 1, d = 1, h = 0, m = 0, s = 0;
			try
			{
				y = Int32.Parse(encodedDate.Substring(0, 4));
				M = Int32.Parse(encodedDate.Substring(5, 2));
				d = Int32.Parse(encodedDate.Substring(8, 2));
				h = Int32.Parse(encodedDate.Substring(11, 2));
				m = Int32.Parse(encodedDate.Substring(14, 2));
				s = Int32.Parse(encodedDate.Substring(17, 2));
			}
			catch (Exception E)
			{ }
			return new DateTime(y, M, d, h, m, s);
		}

		public static void EncodeColonsInFile(string filePath)
		{
			File.WriteAllText(filePath, File.ReadAllText(filePath).Replace(":", ColonReplacement));
		}

		public static string EncodeColons(string text)
		{
			return text.Replace(":", ColonReplacement);
		}

		public static string DecodeColons(string text)
		{
			return text.Replace(ColonReplacement, ":");
		}

		public static XmlAttribute GetNewAttribute(XmlDocument doc, string name, string value)
		{
			XmlAttribute attr = doc.CreateAttribute(name);
			attr.Value = value;
			return attr;
		}

		public static XmlAttribute GetNewAttribute(XmlDocument doc, string name, int value)
		{
			return GetNewAttribute(doc, name, value.ToString());
		}

		public static Size ScaleRectangle(int width, int height, int maxWidth, int maxHeight)
		{
			var ratioX = (double) maxWidth / width;
			var ratioY = (double) maxHeight / height;
			var ratio = Math.Min(ratioX, ratioY);

			var newWidth = (int) (width * ratio);
			var newHeight = (int) (height * ratio);

			return new Size(newWidth, newHeight);
		}

		public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
		{
			Size newSize = ScaleRectangle(image.Width, image.Height, maxWidth, maxHeight);
			Image newImage = new Bitmap(newSize.Width, newSize.Height);
			Graphics.FromImage(newImage).DrawImage(image, 0, 0, newSize.Width, newSize.Height);
			return newImage;
		}

		public static string RegularPlural(string singularForm, int quantity, bool includeQuantity)
		{
			string result = includeQuantity ? quantity + " " + singularForm : singularForm;
			return quantity == 1 ? result : result + "s";
		}

		public static string FormatDateTime(DateTime date, string format)
		{
			return date.ToString(format, CultureInfo.InvariantCulture);
		}

		public static string FormatNumber(long number)
		{
			return number.ToString("#,##0");
		}

		public static string FormatDuration(long seconds)
		{
			return string.Format("{0}:{1}", seconds / 60, (seconds % 60).ToString("00"));
		}

		public static string FormatTimeSpan(TimeSpan timeSpan)
		{
			string result = timeSpan.Hours == 0 ? "" : timeSpan.Hours.ToString() + " hrs ";
			result = timeSpan.Minutes == 0 ? result : result + timeSpan.Minutes.ToString() + " min ";
			result = timeSpan.Seconds == 0 ? result : result + timeSpan.Seconds.ToString() + " sec ";
			return result.Equals("") ? "just now" : result.Substring(0, result.Length - 1);
		}

		public static string FormatElapsedTime(TimeSpan timeSpan)
		{
			if (timeSpan.Ticks < 0)
				return timeSpan.ToString();
			if (timeSpan.Days != 0)
				return timeSpan.Days == 1 ? "one day ago" : timeSpan.Days + " days ago";
			if (timeSpan.Hours != 0)
				return timeSpan.Hours == 1 ? "one hour ago" : timeSpan.Hours + " hours ago";
			if (timeSpan.Minutes != 0)
				return timeSpan.Minutes == 1 ? "one minute ago" : timeSpan.Minutes + " minutes ago";
			if (timeSpan.Seconds != 0)
				return timeSpan.Seconds == 1 ? "one second ago" : timeSpan.Seconds + " seconds ago";
			return "just now";
		}
	}

	public static class Paths
	{
		public const string DatabaseFile = @"database.xml";
		public const string SettingsFile = @"settings.xml";
		public const string DataFolder = @"data\";
		public const string ImageFolder = @"images\";
	}
}
