﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace YoutubeSubscriptions.Classes
{
	public class TDatabase
	{
		public List<TYoutuber> Youtubers;

		public TYoutuber GetYoutuberByID(string ID)
		{
			foreach (TYoutuber youtuber in Youtubers)
				if (youtuber.ID.Equals(ID))
					return youtuber;
			return null;
		}

		public static string OpenDatabase(out TDatabase Database)
		{
			string phase = "reading data from XML: ";
			try
			{
				phase += "initializing the serializer; ";
				XmlSerializer serializer = new XmlSerializer(typeof(TDatabase));
				phase = "initializing the file stream; ";
				FileStream fs = new FileStream(Paths.DatabaseFile, FileMode.Open);
				phase = "attempting deserialization";
				Database = (TDatabase) serializer.Deserialize(fs);
				fs.Close();
			}
			catch (Exception E)
			{
				Database = null;
				return "TDatabase.OpenProject() ERROR: Failed to open database at phase '" + phase + "'.\n\n" + E.ToString();
			}
			return "";
		}

		public static string SaveDatabase(TDatabase Database)
		{
			string phase = "saving data to XML: ";
			try
			{
				phase += "initializing the serializer; ";
				XmlSerializer serializer = new XmlSerializer(typeof(TDatabase));
				phase = "initializing the file stream; ";
				TextWriter writer = new StreamWriter(Paths.DatabaseFile);
				phase = "attempting serialization; ";
				serializer.Serialize(writer, Database);
				writer.Close();
			}
			catch (Exception E)
			{
				return "TDatabase.SaveDatabase() ERROR: Failed to save database at phase '" + phase + "'.\n\n" + E.ToString();
			}
			return "";
		}
	}

	public class TYoutuber
	{
		public string ID;
		public string Name;
		public DateTime InfoLastUpdated;
		public DateTime Joined;
		public long Subscribers;
		public long TotalVideos;
		public long TotalViews;
		public List<TVideo> Videos;
		[XmlIgnore]
		public Image Thumb;

		public TYoutuber()
		{
			this.Thumb = null;
		}

		public string GetYoutuberJsonAddress()
		{
			return string.Format("http://gdata.youtube.com/feeds/api/users/{0}?v=2", this.ID);
		}

		public string GetVideosJsonAddress(int start, int count)
		{
			return string.Format("http://gdata.youtube.com/feeds/api/users/{0}/uploads?v=2&start-index={1}&max-results={2}", this.ID, start, count);
		}

		public string GetImagePath()
		{
			return string.Format(@"{0}\{1}.jpg", Paths.ImageFolder, this.ID);
		}

		public void LoadThumbImageIfExists()
		{
			if (File.Exists(this.GetImagePath()))
			{
				int imgWidth = SubscriptionBox.boxHeight - 2 * SubscriptionBox.imagePadding;
				this.Thumb = Utils.ScaleImage(new Bitmap(this.GetImagePath()), imgWidth, imgWidth);
			}
			else
				this.Thumb = null;
		}
	}

	public class TVideo
	{
		public string ID;
		public DateTime Uploaded;
		public string Title;
		public long Duration;
		public long Views;
		public long Likes;
		public long Dislikes;
		public double AvgRating;
		public long Comments;
		[XmlIgnore]
		public Image Thumb;

		public TVideo()
		{
			this.Thumb = null;
		}

		public string GetImagePath()
		{
			return string.Format(@"{0}\{1}.jpg", Paths.ImageFolder, this.ID);
		}

		public string GetWebpageVideoURL()
		{
			return string.Format(@"https://www.youtube.com/watch?v={0}", this.ID);
		}

		public string GetVideoURL()
		{
			return string.Format(@"https://www.youtube.com/v/{0}", this.ID);
		}

		public void LoadThumbImageIfExists()
		{
			if (File.Exists(this.GetImagePath()))
			{
				Size maxSize = VideoBox.thumbnailMaxSize;
				this.Thumb = Utils.ScaleImage(new Bitmap(this.GetImagePath()), maxSize.Width, maxSize.Height);
			}
			else
				this.Thumb = null;
		}
	}

	///
	///
	///

	public class TSettings
	{
		public static string[] UploadedFormats = { "dd/MM/yy", "d/MM/yyyy", "d-MMM-yyyy", "d MMMM yyyy", "ddd, d MMMM yyyy", "dddd, d MMMM yyyy" };
		public int VideoDaysGoBack;
		public int VideoColumns;
		public int UploadedFormatIndex;
		public bool ShowVideoTitle;
		public bool ShowVideoStats;

		public void ReadFromFile()
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(Paths.SettingsFile);
				XmlNode node = doc.SelectSingleNode("/SETTINGS");

				try { VideoDaysGoBack = Int32.Parse(node["VideoDaysGoBack"].Attributes["value"].Value); }
				catch (Exception E) { VideoDaysGoBack = 31; }

				try { VideoColumns = Int32.Parse(node["VideoColumns"].Attributes["value"].Value); }
				catch (Exception E) { VideoColumns = 4; }

				try { UploadedFormatIndex = Int32.Parse(node["UploadedFormatIndex"].Attributes["value"].Value); }
				catch (Exception E) { UploadedFormatIndex = 0; }

				try { ShowVideoTitle = Boolean.Parse(node["ShowVideoTitle"].Attributes["value"].Value); }
				catch (Exception E) { ShowVideoTitle = true; }

				try { ShowVideoStats = Boolean.Parse(node["ShowVideoStats"].Attributes["value"].Value); }
				catch (Exception E) { ShowVideoStats = true; }
			}
			catch (Exception E)
			{
				MessageBox.Show(E.ToString(), "TSettings.ReadFromFile() ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		public void SaveToFile()
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				XmlNode root, node;

				root = doc.AppendChild(doc.CreateElement("SETTINGS"));
				root.Attributes.Append(Utils.GetNewAttribute(doc, "lastSavedCirca", DateTime.Now.ToString("ddd, d MMMM yyyy, HH:mm")));

				node = root.AppendChild(doc.CreateElement("VideoDaysGoBack"));
				node.Attributes.Append(Utils.GetNewAttribute(doc, "value", VideoDaysGoBack.ToString()));

				node = root.AppendChild(doc.CreateElement("VideoColumns"));
				node.Attributes.Append(Utils.GetNewAttribute(doc, "value", VideoColumns.ToString()));

				node = root.AppendChild(doc.CreateElement("UploadedFormatIndex"));
				node.Attributes.Append(Utils.GetNewAttribute(doc, "value", UploadedFormatIndex.ToString()));

				node = root.AppendChild(doc.CreateElement("ShowVideoTitle"));
				node.Attributes.Append(Utils.GetNewAttribute(doc, "value", ShowVideoTitle.ToString()));

				node = root.AppendChild(doc.CreateElement("ShowVideoStats"));
				node.Attributes.Append(Utils.GetNewAttribute(doc, "value", ShowVideoStats.ToString()));

				doc.Save(Paths.SettingsFile);
			}
			catch (Exception E)
			{
				MessageBox.Show(E.ToString(), "TSettings.SaveToFile() ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}
