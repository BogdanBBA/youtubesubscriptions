﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YoutubeSubscriptions.Classes
{
	public class SubscriptionVideoManager
	{
		public TYoutuber Youtuber;
		private Panel Container;
		private EventHandler Click;
		public List<VideoBox> VideoBoxes;
		public VideoBox SelectedVideoBox;

		public SubscriptionVideoManager(TYoutuber Youtuber, Panel Container, EventHandler Click)
		{
			this.Youtuber = Youtuber;
			this.Container = Container;
			this.Click += Click;
			this.VideoBoxes = new List<VideoBox>();
			this.RecreateVideoBoxes();
			this.SelectedVideoBox = this.VideoBoxes[0];
			VideoBox.DrawVideoBox(this.SelectedVideoBox, false);
		}

		public void RecreateVideoBoxes()
		{
			TSettings sett = ((FMain) Application.OpenForms[0]).Settings;
			for (int i = VideoBoxes.Count; i < Youtuber.Videos.Count; i++)
				VideoBoxes.Add(new VideoBox(null, Container, new Rectangle(0, 0, 1, 1), Click));
			int vbWidth = (Container.Width - (sett.VideoColumns - 1) * FMain.ControlPadding - 20) / sett.VideoColumns;
			int vbHeight = (int) Math.Round((double) (vbWidth * 9) / 16);
			VideoBox.titleHeight = VideoBox.titleLineHeight * ((sett.VideoColumns + 1) / 2);
			vbHeight = vbHeight + (sett.ShowVideoTitle ? VideoBox.titleHeight : 0) + (sett.ShowVideoStats ? VideoBox.statsHeight : 0);
			VideoBox.videoBoxSize = new Size(vbWidth, vbHeight);
			VideoBox.thumbnailMaxSize = new Size(VideoBox.videoBoxSize.Width, (int) Math.Round((double) (VideoBox.videoBoxSize.Width * 9) / 16) + 1);
			Container.VerticalScroll.Value = 0;
			for (int i = 0, lastX = 0, lastY = 0; i < VideoBoxes.Count; i++)
			{
				if (i < Youtuber.Videos.Count)
				{
					VideoBoxes[i].SetBounds(lastX, lastY, VideoBox.videoBoxSize.Width, VideoBox.videoBoxSize.Height);
					VideoBoxes[i].Video = Youtuber.Videos[i];
					VideoBoxes[i].Video.LoadThumbImageIfExists();
					VideoBox.DrawVideoBox(VideoBoxes[i], false);
					lastX += VideoBox.videoBoxSize.Width + FMain.ControlPadding;
					if (lastX + VideoBox.videoBoxSize.Width + 19 > Container.Width)
					{
						lastX = 0;
						lastY += VideoBox.videoBoxSize.Height + FMain.ControlPadding;
					}
				}
				VideoBoxes[i].Visible = i < Youtuber.Videos.Count;
			}
		}
	}

	public class VideoBox : PictureBox
	{
		public static Size videoBoxSize = new Size(100, 100);
		public static Size thumbnailMaxSize = new Size(100, 100);
		public const int titleLineHeight = 22;
		public static int titleHeight = titleLineHeight;
		public const int statsHeight = 24;
		public const int textPadding = 2;
		private static Brush bgBrush = new SolidBrush(MyGuiComponents.ButtonC);
		private static Brush bgBrushMO = new SolidBrush(MyGuiComponents.SelectedButtonC);
		private static Brush titleBrush = new SolidBrush(MyGuiComponents.FontC);
		private static Brush titleBrushMO = new SolidBrush(Color.Orange);
		private static Brush statsBrush = new SolidBrush(MyGuiComponents.FontC);
		private static Brush statsBrushMO = new SolidBrush(MyGuiComponents.FontC);
		private static Brush durationBgBrush = new SolidBrush(Color.FromArgb(191, Color.Black));

		private static Font titleFont = MyGuiComponents.GetFont("Segoe UI", 10, false);
		private static Font durationFont = MyGuiComponents.GetFont("Andika", 11, false);
		private static Font statsFont = MyGuiComponents.GetFont("Andika", 10, false);

		public TVideo Video;

		public VideoBox(TVideo Video, Panel container, Rectangle bounds, EventHandler Click)
			: base()
		{
			this.Video = Video;
			this.Parent = container;
			this.SetBounds(bounds.Left, bounds.Top, bounds.Width, bounds.Height);
			this.Cursor = Cursors.Hand;
			this.Click += Click;
			this.MouseEnter += new EventHandler(OnMouseEnter);
			this.MouseLeave += new EventHandler(OnMouseLeave);
			DrawVideoBox(this, false);
		}

		public static void OnMouseEnter(object sender, EventArgs e)
		{ DrawVideoBox((VideoBox) sender, true); }

		public static void OnMouseLeave(object sender, EventArgs e)
		{ DrawVideoBox((VideoBox) sender, false); }

		public static void DrawVideoBox(VideoBox pic, bool selected)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);
			TSettings sett = ((FMain) Application.OpenForms[0]).Settings;
			Brush brush = selected ? bgBrushMO : bgBrush;
			Brush titleBr = selected ? titleBrushMO : titleBrush, statsBr = selected ? statsBrushMO : statsBrush;
			string text;
			Point location;

			g.FillRectangle(brush, 0, 0, pic.Width, pic.Height);
			if (pic.Video == null)
				return;

			if (pic.Video.Thumb != null)
				try { g.DrawImage(pic.Video.Thumb, new Point(thumbnailMaxSize.Width / 2 - pic.Video.Thumb.Width / 2, thumbnailMaxSize.Height / 2 - pic.Video.Thumb.Height / 2)); }
				catch (Exception E) { }

			text = Utils.FormatDuration(pic.Video.Duration);
			Size size = g.MeasureString(text, durationFont).ToSize();
			location = new Point(thumbnailMaxSize.Width - size.Width, thumbnailMaxSize.Height - size.Height - 1);
			Rectangle rectangle = new Rectangle(location, size);
			g.FillRectangle(durationBgBrush, rectangle);
			g.DrawString(text, durationFont, Brushes.White, location);

			if (sett.ShowVideoTitle)
			{
				Rectangle rect = new Rectangle(textPadding, thumbnailMaxSize.Height + textPadding, thumbnailMaxSize.Width - 2 * textPadding, titleHeight);
				g.DrawString(pic.Video.Title, titleFont, titleBr, rect);
			}

			if (sett.ShowVideoStats)
			{
				int top = thumbnailMaxSize.Height + (sett.ShowVideoTitle ? titleHeight : 0) + textPadding;

				text = Utils.FormatDateTime(pic.Video.Uploaded, TSettings.UploadedFormats[sett.UploadedFormatIndex]);
				location = new Point(textPadding, top);
				g.DrawString(text, statsFont, statsBr, location);

				text = Utils.FormatNumber(pic.Video.Views) + " views";
				location = new Point(thumbnailMaxSize.Width - g.MeasureString(text, statsFont).ToSize().Width - textPadding, top);
				g.DrawString(text, statsFont, statsBr, location);
			}

			pic.Image = bmp;
		}
	}
}
