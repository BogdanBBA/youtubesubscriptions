﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Xml;
using System.Threading;

namespace YoutubeSubscriptions.Classes
{
	public class SubscriptionManager
	{
		public TDatabase Database;
		private Panel Container;
		private EventHandler Click;
		public List<SubscriptionBox> SubscriptionBoxes;
		public SubscriptionBox SelectedSubBox;

		public SubscriptionManager(TDatabase Database, Panel Container, EventHandler Click)
		{
			this.Database = Database;
			this.Container = Container;
			this.Click = Click;
			this.SubscriptionBoxes = new List<SubscriptionBox>();
			this.RecreateSubscriptionBoxes();
			this.SelectedSubBox = this.SubscriptionBoxes[0];
			this.SelectedSubBox.Checked = true;
			SubscriptionBox.DrawSubscriptionBox(this.SelectedSubBox, false, null);
		}

		public void RecreateSubscriptionBoxes()
		{
			for (int i = SubscriptionBoxes.Count; i < Database.Youtubers.Count; i++)
				SubscriptionBoxes.Add(new SubscriptionBox(null, Container, new Rectangle(0, i * SubscriptionBox.boxHeight, Container.Width - 20, SubscriptionBox.boxHeight), Click));
			for (int i = 0; i < SubscriptionBoxes.Count; i++)
			{
				SubscriptionBoxes[i].Visible = i < Database.Youtubers.Count;
				if (SubscriptionBoxes[i].Visible)
				{
					SubscriptionBoxes[i].Youtuber = Database.Youtubers[i];
					SubscriptionBoxes[i].Youtuber.LoadThumbImageIfExists();
					SubscriptionBox.DrawSubscriptionBox(SubscriptionBoxes[i], false, null);
				}
			}
		}

		public int GetThreadsWithStatus(int status)
		{
			int n = 0;
			foreach (SubscriptionBox sb in SubscriptionBoxes)
				if (sb.CurrentStatus == status)
					n++;
			return n;
		}

		public void UpdateAllSubscriptions()
		{
			foreach (SubscriptionBox sb in SubscriptionBoxes)
				if (sb.Visible && sb.CurrentStatus != SubscriptionBox.StatusUpdating)
					sb.UpdateThread.RunWorkerAsync();
		}
	}

	public class SubscriptionBox : PictureBox
	{
		public const int StatusReady = 0, StatusUpdating = 1, StatusFailed = 2;
		public const int boxHeight = 60, imagePadding = 5, checkedBarHeight = 2;
		private static Brush bgBrush = new SolidBrush(MyGuiComponents.ButtonC);
		private static Brush bgBrushMO = new SolidBrush(MyGuiComponents.SelectedButtonC);
		private static Brush textBrush = new SolidBrush(MyGuiComponents.FontC);
		private static Brush checkedBrush = new SolidBrush(Color.Orange);

		private static Font nameFont = MyGuiComponents.GetFont("Andika", 17, true);
		private static Font statusFont = MyGuiComponents.GetFont("Andika", 12, false);

		public TYoutuber Youtuber;
		public bool Checked;

		public BackgroundWorker UpdateThread;
		public int CurrentStatus;
		public string FailureReason;

		public SubscriptionBox(TYoutuber Youtuber, Panel container, Rectangle bounds, EventHandler Click)
			: base()
		{
			this.Youtuber = Youtuber;
			this.Checked = false;
			this.UpdateThread = new BackgroundWorker();
			this.UpdateThread.DoWork += new DoWorkEventHandler(UpdateSubscription);
			this.UpdateThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(UpdateYoutuberFinished);
			this.CurrentStatus = StatusReady;
			this.FailureReason = "(you shouldn't be seeing this)";
			this.Parent = container;
			this.SetBounds(bounds.Left, bounds.Top, bounds.Width, bounds.Height);
			this.Cursor = Cursors.Hand;
			this.Click += Click;
			this.MouseEnter += new EventHandler(OnMouseEnter);
			this.MouseLeave += new EventHandler(OnMouseLeave);
			DrawSubscriptionBox(this, false, null);
		}

		private void UpdateSubscription(object sender, DoWorkEventArgs e)
		{
			this.CurrentStatus = StatusUpdating;
			this.FailureReason = "";

			string phase = "preparing";
			XmlDocument doc = new XmlDocument();

			// Youtuber info

			// download youtuber info
			string destFile = Path.Combine(Paths.DataFolder, this.Youtuber.ID + ".xml");
			if (!DownloadFromTheHolyInternet(this.Youtuber.GetYoutuberJsonAddress(), destFile))
			{
				this.FailureReason = "Failed to download youtuber JSON from\n" + this.Youtuber.GetYoutuberJsonAddress();
				return;
			}
			// decode youtuber info
			try
			{
				phase = "loading as XML";
				Utils.EncodeColonsInFile(destFile);
				doc.Load(destFile);
				foreach (XmlNode iNode in doc.ChildNodes[1].ChildNodes)
					if (iNode.Name.Equals(Utils.EncodeColons("gd:feedLink")))
					{
						phase = "decoding XML (upload count)";
						if (iNode.Attributes["rel"] != null && iNode.Attributes["rel"].Value.IndexOf("user.uploads") != -1)
							this.Youtuber.TotalVideos = long.Parse(iNode.Attributes["countHint"].Value);
					}
					else if (iNode.Name.Equals(Utils.EncodeColons("yt:statistics")))
					{
						phase = "decoding XML (subs): ";
						this.Youtuber.Subscribers = long.Parse(iNode.Attributes["subscriberCount"].Value);
						phase = "decoding XML (total views)";
						this.Youtuber.TotalViews = long.Parse(iNode.Attributes["totalUploadViews"].Value);
					}
					else if (iNode.Name.Equals("published"))
					{
						phase = "decoding XML (joined)";
						this.Youtuber.Joined = Utils.DecodeYoutubeApiDate(iNode.InnerText);
					}
					else if (iNode.Name.Equals(Utils.EncodeColons("media:thumbnail")))
					{
						phase = "downloading youtuber profile pic";
						string src = Utils.DecodeColons(iNode.Attributes["url"].Value);
						string dest = Path.Combine(Paths.ImageFolder, this.Youtuber.ID + ".jpg");
						DownloadFromTheHolyInternet(src, dest);
					}
			}
			catch (Exception E)
			{ this.FailureReason = "Failed to load/decode the downloaded XML youtuber information from \"" + destFile + "\".\n\nPhase: " + phase + "\n\n" + E.ToString(); }

			// Video info

			// reset videos
			this.Youtuber.Videos.Clear();
			int downloadStartIndex = 1, downloadCount = 50, iCurr = 0;
			DateTime earliestUploadDate = DateTime.Now.Date.AddDays(-((FMain) Application.OpenForms[0]).Settings.VideoDaysGoBack);
			bool itsTimeToQuit = false;

			while (true)
			{
				// download video info
				destFile = Path.Combine(Paths.DataFolder, this.Youtuber.ID + " videos " + downloadStartIndex + "-" + (downloadStartIndex + downloadCount - 1) + ".xml");
				if (!DownloadFromTheHolyInternet(this.Youtuber.GetVideosJsonAddress(downloadStartIndex, downloadCount), destFile))
				{
					this.FailureReason = "Failed to download video uploads JSON from\n" + this.Youtuber.GetVideosJsonAddress(downloadStartIndex, downloadCount);
					return;
				}

				// decode video info
				try
				{
					phase = "loading as XML";
					Utils.EncodeColonsInFile(destFile);
					doc.Load(destFile);
					phase = "parsing XML";
					foreach (XmlNode iNode in doc.ChildNodes[1].ChildNodes)
						if (iNode.Name.Equals("entry"))
						{
							TVideo video = new TVideo();
							foreach (XmlNode jNode in iNode.ChildNodes)
								if (jNode.Name.Equals("id"))
								{
									phase = "decoding id";
									video.ID = jNode.InnerText.Substring(jNode.InnerText.IndexOf(Utils.EncodeColons(":video:")) + Utils.EncodeColons(":video:").Length);
								}
								else if (jNode.Name.Equals("title"))
								{
									phase = "decoding title";
									video.Title = Utils.DecodeColons(jNode.InnerText.Replace("  ", " ").Trim());
								}
								else if (jNode.Name.Equals(Utils.EncodeColons("gd:comments")))
								{
									phase = "decoding comment count";
									foreach (XmlNode kNode in jNode.ChildNodes)
										if (kNode.Name.Equals(Utils.EncodeColons("gd:feedLink")))
											video.Comments = long.Parse(kNode.Attributes["countHint"].Value);
								}
								else if (jNode.Name.Equals(Utils.EncodeColons("media:group")))
								{
									phase = "decoding screen url";
									foreach (XmlNode kNode in jNode.ChildNodes)
										if (kNode.Name.Equals(Utils.EncodeColons("media:thumbnail")) && kNode.Attributes["url"] != null && kNode.Attributes["url"].Value.IndexOf("mqdefault.jpg") != -1)
										{
											phase = "downloading video screenshot";
											string src = Utils.DecodeColons(kNode.Attributes["url"].Value);
											string dest = Path.Combine(Paths.ImageFolder, video.ID + ".jpg");
											DownloadFromTheHolyInternet(src, dest);
										}
										else if (kNode.Name.Equals(Utils.EncodeColons("yt:duration")))
										{
											phase = "decoding duration";
											video.Duration = long.Parse(kNode.Attributes["seconds"].Value);
										}
										else if (kNode.Name.Equals(Utils.EncodeColons("yt:uploaded")))
										{
											phase = "decoding uploaded date";
											video.Uploaded = Utils.DecodeYoutubeApiDate(kNode.InnerText);
										}
								}
								else if (jNode.Name.Equals(Utils.EncodeColons("gd:rating")))
								{
									phase = "decoding rating";
									video.AvgRating = Double.Parse(jNode.Attributes["average"].Value);
								}
								else if (jNode.Name.Equals(Utils.EncodeColons("yt:statistics")))
								{
									phase = "decoding views";
									video.Views = long.Parse(jNode.Attributes["viewCount"].Value);
								}
								else if (jNode.Name.Equals(Utils.EncodeColons("yt:rating")))
								{
									phase = "decoding likes/dislikes";
									video.Likes = long.Parse(jNode.Attributes["numLikes"].Value);
									video.Dislikes = long.Parse(jNode.Attributes["numDislikes"].Value);
								}
							//
							if (video.Uploaded.CompareTo(earliestUploadDate) >= 0)
								this.Youtuber.Videos.Add(video);
							//
							iCurr++;
							//MessageBox.Show(string.Format("{4}\nid={0}, date={1}/{5}\nicurr={2}/total={3}", video.ID, video.Uploaded.ToString("dd-MMMM-yyyy"), iCurr, this.Youtuber.TotalVideos, this.Youtuber.Name, earliestUploadDate.ToString("dd-MMMM-yyyy")));
							if (iCurr == this.Youtuber.TotalVideos || video.Uploaded.CompareTo(earliestUploadDate) < 0)
								itsTimeToQuit = true;
							if (itsTimeToQuit)
								break;
						}

					if (itsTimeToQuit)
						break;
				}
				catch (Exception E)
				{ this.FailureReason = "Failed to load/decode the downloaded XML video list information from \"" + destFile + "\", range " + downloadStartIndex + "+" + downloadCount + ".\n\nPhase: " + phase + "\n\n" + E.ToString(); }

				downloadStartIndex += downloadCount;
			}

			// sort videos
			if (this.Youtuber.Videos.Count >= 2)
				for (int i = 0; i < this.Youtuber.Videos.Count - 1; i++)
					for (int j = i + 1; j < this.Youtuber.Videos.Count; j++)
						if (this.Youtuber.Videos[i].Uploaded.CompareTo(this.Youtuber.Videos[j].Uploaded) < 0)
						{
							TVideo aux = this.Youtuber.Videos[i];
							this.Youtuber.Videos[i] = this.Youtuber.Videos[j];
							this.Youtuber.Videos[j] = aux;
						}

			this.Youtuber.InfoLastUpdated = DateTime.Now;
		}

		private static bool DownloadFromTheHolyInternet(string url, string destFile)
		{
			try { new WebClient().DownloadFile(url, destFile); }
			catch (Exception E) { return false; }
			return true;
		}

		private void UpdateYoutuberFinished(object sender, RunWorkerCompletedEventArgs e)
		{
			//MessageBox.Show(this.Youtuber.Name + ": " + this.Youtuber.Videos.Count);
			this.CurrentStatus = this.FailureReason.Equals("") ? StatusReady : StatusFailed;
			if (this.CurrentStatus == StatusFailed)
				MessageBox.Show(this.Youtuber.ID + " failed!\n\nReason: " + FailureReason);
			foreach (SubscriptionBox sb in ((FMain) Application.OpenForms[0]).SubManager.SubscriptionBoxes)
				if (sb.Visible && sb.Youtuber.ID.Equals(this.Youtuber.ID))
				{
					sb.Youtuber.LoadThumbImageIfExists();
					if (sb.Checked)
						sb.InvokeOnClick(sb, null);
					SubscriptionBox.DrawSubscriptionBox(sb, false, null);
					break;
				}
		}

		public static void OnMouseEnter(object sender, EventArgs e)
		{ if (!((SubscriptionBox) sender).UpdateThread.IsBusy) DrawSubscriptionBox((SubscriptionBox) sender, true, null); }

		public static void OnMouseLeave(object sender, EventArgs e)
		{ if (!((SubscriptionBox) sender).UpdateThread.IsBusy) DrawSubscriptionBox((SubscriptionBox) sender, false, null); }

		public static void DrawSubscriptionBox(SubscriptionBox pic, bool selected, string status)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);
			Brush brush = selected ? bgBrushMO : bgBrush;
			g.FillRectangle(brush, 0, 0, pic.Width, pic.Height);
			if (pic.Youtuber == null)
				return;
			if (pic.Youtuber.Thumb != null)
				try
				{
					int imgWidth = pic.Height - 2 * imagePadding;
					g.DrawImage(pic.Youtuber.Thumb, new Point(imagePadding, imagePadding));
				}
				catch (Exception E) { }
			Size size = g.MeasureString(pic.Youtuber.Name, nameFont).ToSize();
			g.DrawString(pic.Youtuber.Name, nameFont, textBrush, new Point(pic.Height, 0));
			string text = Utils.RegularPlural("recent video", pic.Youtuber.Videos.Count, true);
			g.DrawString(text, statusFont, textBrush, new Point(pic.Height, pic.Height - g.MeasureString(text, statusFont).ToSize().Height - checkedBarHeight - 2));
			if (pic.Checked)
				g.FillRectangle(Brushes.Orange, 0, pic.Height - checkedBarHeight - 1, pic.Width, pic.Height - 1);
			pic.Image = bmp;
		}
	}
}
