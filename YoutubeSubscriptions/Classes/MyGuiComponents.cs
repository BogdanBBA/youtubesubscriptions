﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YoutubeSubscriptions.Classes
{
	public static class MyGuiComponents
	{
		public static Color FormBackgroundC = ColorTranslator.FromHtml("#222222");
		public static Color ButtonC = ColorTranslator.FromHtml("#222222");
		public static Color SelectedButtonC = ColorTranslator.FromHtml("#333333");
		public static Color FontC = ColorTranslator.FromHtml("#FFFFFF");
		public static Color SelectedFontC = Color.Orange;

		public static Font GetFont(string name, int size, bool bold)
		{
			return new Font(name, size, bold ? FontStyle.Bold : FontStyle.Regular);
		}

		public static void InitializeAndFormatFormComponents(Form form)
		{
			form.BackColor = FormBackgroundC;

			foreach (Control ctrl in form.Controls)
				if (ctrl is Label)
					((Label) ctrl).BackColor = form.BackColor;
				else if (ctrl is Panel)
					((Panel) ctrl).BackColor = form.BackColor;
				else if (ctrl is PictureBox)
					((PictureBox) ctrl).BackColor = form.BackColor;
		}

		public static List<PictureBoxButton> CreateMenuButtons(Panel container, List<string> captions, bool horizontal, EventHandler click)
		{
			List<PictureBoxButton> result = new List<PictureBoxButton>();
			for (int i = 0, n = captions.Count, dim = horizontal ? container.Width / n : container.Height / n; i < n; i++)
			{
				PictureBoxButton pic = new PictureBoxButton(captions[i], click);
				pic.Parent = container;
				pic.Cursor = Cursors.Hand;
				if (horizontal)
					pic.SetBounds(i * dim, 0, dim, container.Height);
				else
					pic.SetBounds(0, i * dim, container.Width, dim);
				PictureBoxButton.OnMouseLeave(pic, null);
				result.Add(pic);
			}
			return result;
		}

		public static void DrawLikesDislikes(PictureBox pic, long likes, long dislikes)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);
			Brush brush = new SolidBrush(FormBackgroundC);
			g.FillRectangle(brush, 0, 0, pic.Width, pic.Height);
			if (likes > 0)
			{
				brush = new SolidBrush(ColorTranslator.FromHtml("#95E072"));
				long width = (long) (double) (pic.Width * likes) / (likes + dislikes);
				g.FillRectangle(brush, 0, 0, width, pic.Height);
			}
			if (dislikes > 0)
			{
				brush = new SolidBrush(ColorTranslator.FromHtml("#D4576B"));
				long width = (long) (double) (pic.Width * dislikes) / (likes + dislikes);
				g.FillRectangle(brush, pic.Width - width - 1, 0, width, pic.Height);
			}
			pic.Image = bmp;
		}

		public static void DrawViewsSubscribersPercentage(PictureBox pic, long views, long subscribers)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);
			Brush brush = new SolidBrush(FormBackgroundC);
			g.FillRectangle(brush, 0, 0, pic.Width, pic.Height);
			brush = new SolidBrush(ColorTranslator.FromHtml("#0F56A3"));
			long width = views >= subscribers ? pic.Width : (long) (double) (pic.Width * views) / subscribers;
			g.FillRectangle(brush, 0, 0, width, pic.Height);
			pic.Image = bmp;
		}
	}

	/// 
	/// 
	/// 
	public class PictureBoxButton : PictureBox
	{
		public string Information;

		public PictureBoxButton(string Information, EventHandler Click)
			: this(Information, Click, OnMouseEnter, OnMouseLeave)
		{
		}

		public PictureBoxButton(string Information, EventHandler Click, EventHandler MouseEnter, EventHandler MouseLeave)
			: base()
		{
			this.Information = Information;
			this.Click += Click;
			this.MouseEnter += MouseEnter;
			this.MouseLeave += MouseLeave;
			MouseLeave(this, null);
		}

		public static void OnMouseEnter(object sender, EventArgs e)
		{
			DrawPictureBoxButton((PictureBoxButton) sender, true);
		}

		public static void OnMouseLeave(object sender, EventArgs e)
		{
			DrawPictureBoxButton((PictureBoxButton) sender, false);
		}

		private static void DrawPictureBoxButton(PictureBoxButton pic, bool selected)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);
			Brush bgBrush = selected ? new SolidBrush(MyGuiComponents.SelectedButtonC) : new SolidBrush(MyGuiComponents.ButtonC);
			Brush textBrush = selected ? new SolidBrush(MyGuiComponents.SelectedFontC) : new SolidBrush(MyGuiComponents.FontC);
			Font font = MyGuiComponents.GetFont("Segoe UI", 17, true);
			g.FillRectangle(bgBrush, 0, 0, pic.Width, pic.Height);
			if (selected)
				g.DrawRectangle(new Pen(Color.WhiteSmoke), 0, 0, pic.Width - 1, pic.Height - 1);
			Size size = g.MeasureString(pic.Information, font).ToSize();
			g.DrawString(pic.Information, font, textBrush, new Point(pic.Width / 2 - size.Width / 2, pic.Height / 2 - size.Height / 2));
			pic.Image = bmp;
		}
	}

	/// 
	/// 
	/// 

	public class PictureBoxStatusBar : PictureBox
	{
		private static Brush bgBrush = new SolidBrush(MyGuiComponents.FormBackgroundC);
		private static Brush statusTextBrush = new SolidBrush(Color.WhiteSmoke);
		private static Brush progressPassedBrush = new SolidBrush(Color.GreenYellow);
		private static Brush progressRemainingBrush = new SolidBrush(Color.DarkGray);
		private static Font font = MyGuiComponents.GetFont("Segoe UI", 13, true);
		private const int barPadding = 11;

		public string Information;
		public int Percentage;

		public PictureBoxStatusBar(Panel container, string Information, int Percentage)
			: base()
		{
			this.Information = Information;
			this.Percentage = Percentage;
			this.Parent = container;
			this.SetBounds(0, 0, container.Width, container.Height);
			RefreshStatusBar();
		}

		public void UpdateStatus(string Information, int Percentage)
		{
			this.Information = Information;
			this.Percentage = Percentage;
			this.RefreshStatusBar();
		}

		private void RefreshStatusBar()
		{
			if (this.Image != null)
				this.Image.Dispose();
			Bitmap bmp = new Bitmap(this.Width, this.Height);
			Graphics g = Graphics.FromImage(bmp);
			g.FillRectangle(bgBrush, 0, 0, this.Width, this.Height);
			g.FillRectangle(progressRemainingBrush, new Rectangle(0, barPadding, this.Width / 4, this.Height - 2 * barPadding));
			g.FillRectangle(progressPassedBrush, new Rectangle(0, barPadding, (int) (double) ((this.Width / 4) * this.Percentage) / 100, this.Height - 2 * barPadding));
			Size size = g.MeasureString(this.Information, font).ToSize();
			g.DrawString(this.Information, font, statusTextBrush, new Point(this.Width / 4 + 8, this.Height / 2 - size.Height / 2));
			this.Image = bmp;
		}
	}
}
