﻿namespace YoutubeSubscriptions
{
	partial class FMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMain));
			this.MenuP = new System.Windows.Forms.Panel();
			this.SubListP = new System.Windows.Forms.Panel();
			this.VideoListP = new System.Windows.Forms.Panel();
			this.StatusP = new System.Windows.Forms.Panel();
			this.UpdateAllTimer = new System.Windows.Forms.Timer(this.components);
			this.UpdateCurrentTimer = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// MenuP
			// 
			this.MenuP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.MenuP.Location = new System.Drawing.Point(42, 35);
			this.MenuP.Name = "MenuP";
			this.MenuP.Size = new System.Drawing.Size(200, 100);
			this.MenuP.TabIndex = 0;
			// 
			// SubListP
			// 
			this.SubListP.AutoScroll = true;
			this.SubListP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.SubListP.Location = new System.Drawing.Point(96, 204);
			this.SubListP.Name = "SubListP";
			this.SubListP.Size = new System.Drawing.Size(200, 100);
			this.SubListP.TabIndex = 1;
			// 
			// VideoListP
			// 
			this.VideoListP.AutoScroll = true;
			this.VideoListP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.VideoListP.Location = new System.Drawing.Point(435, 152);
			this.VideoListP.Name = "VideoListP";
			this.VideoListP.Size = new System.Drawing.Size(200, 100);
			this.VideoListP.TabIndex = 2;
			// 
			// StatusP
			// 
			this.StatusP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.StatusP.Location = new System.Drawing.Point(257, 84);
			this.StatusP.Name = "StatusP";
			this.StatusP.Size = new System.Drawing.Size(200, 100);
			this.StatusP.TabIndex = 3;
			// 
			// UpdateAllTimer
			// 
			this.UpdateAllTimer.Interval = 500;
			this.UpdateAllTimer.Tick += new System.EventHandler(this.UpdateTimer_Tick);
			// 
			// UpdateCurrentTimer
			// 
			this.UpdateCurrentTimer.Interval = 500;
			this.UpdateCurrentTimer.Tick += new System.EventHandler(this.UpdateCurrentTimer_Tick);
			// 
			// FMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlDark;
			this.ClientSize = new System.Drawing.Size(756, 428);
			this.Controls.Add(this.StatusP);
			this.Controls.Add(this.VideoListP);
			this.Controls.Add(this.SubListP);
			this.Controls.Add(this.MenuP);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FMain";
			this.Text = "Youtube subscriptions";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.FMain_Load);
			this.Shown += new System.EventHandler(this.FMain_Shown);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel MenuP;
		private System.Windows.Forms.Panel SubListP;
		private System.Windows.Forms.Panel VideoListP;
		private System.Windows.Forms.Panel StatusP;
		private System.Windows.Forms.Timer UpdateAllTimer;
		private System.Windows.Forms.Timer UpdateCurrentTimer;
	}
}

