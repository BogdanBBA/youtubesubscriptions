﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YoutubeSubscriptions.Classes;

namespace YoutubeSubscriptions.Forms
{
	public partial class FVideo : Form
	{
		private static List<string> MenuButtonCaptions = new List<string>(new string[] { "Open webpage", "Open video", "CLOSE" });

		private FMain MainForm;

		private List<PictureBoxButton> MenuButtons;

		private TYoutuber Youtuber;
		private TVideo Video;

		public FVideo(FMain MainForm)
		{
			InitializeComponent();
			this.MainForm = MainForm;
			MyGuiComponents.InitializeAndFormatFormComponents(this);
		}

		private void FVideo_Load(object sender, EventArgs e)
		{
			MenuButtons = MyGuiComponents.CreateMenuButtons(MenuP, MenuButtonCaptions, true, MenuButton_Click);
		}

		public void RefreshInfo(TYoutuber Youtuber, TVideo Video)
		{
			this.Youtuber = Youtuber;
			this.Video = Video;
			try { ThumbnailPB.Image = new Bitmap(Video.GetImagePath()); }
			catch (Exception E) { ThumbnailPB.Image = null; }
			youtuberL.Text = Youtuber.Name;
			videoTitleL.Text = Video.Title;
			uploadedL.Text = string.Format("Uploaded {0} ({1})", Utils.FormatDateTime(Video.Uploaded, "d MMMM yyyy, HH:mm"), Utils.FormatElapsedTime(DateTime.Now.Subtract(Video.Uploaded)));
			durationL.Text = Utils.FormatDuration(Video.Duration);
			ratingL.Text = Utils.FormatNumber(Video.Likes) + " / " + Utils.FormatNumber(Video.Dislikes);
			MyGuiComponents.DrawLikesDislikes(likesDislikesPB, Video.Likes, Video.Dislikes);
			commentsL.Text = Utils.FormatNumber(Video.Comments);
			viewsL.Text = string.Format("{0} ({1}%)", Utils.FormatNumber(Video.Views), ((double) (Video.Views * 100) / Youtuber.Subscribers).ToString("0.0"));
			MyGuiComponents.DrawViewsSubscribersPercentage(viewsSubsPB, Video.Views, Youtuber.Subscribers);
		}

		private void MenuButton_Click(object sender, EventArgs e)
		{
			int r = MenuButtonCaptions.IndexOf(((PictureBoxButton) sender).Information);
			switch (r)
			{
				case 0: // open webpage
					System.Diagnostics.Process.Start(Video.GetWebpageVideoURL());
					break;
				case 1: // open video
					System.Diagnostics.Process.Start(Video.GetVideoURL());
					break;
				case 2: // close
					MainForm.Focus();
					break;
			}
			this.Hide();
		}

		private void youtuberL_Click(object sender, EventArgs e)
		{
			MessageBox.Show("something yummy");
		}
	}
}
