﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YoutubeSubscriptions.Classes;
using YoutubeSubscriptions.Forms;

namespace YoutubeSubscriptions
{
	public partial class FMain : Form
	{
		public const int ControlPadding = 16;
		private static List<string> MenuButtonCaptions = new List<string>() { "UPDATE ALL", "UPDATE CURRENT", "Edit subscriptions", "Settings / about", "EXIT" };

		private List<PictureBoxButton> MenuButtons;
		private PictureBoxStatusBar StatusBar;

		public FSettings SettingsForm;
		public FSubEditor SubEditorForm;
		public FVideo VideoForm;

		public TDatabase Database;
		public TSettings Settings;
		public SubscriptionManager SubManager;
		public SubscriptionVideoManager SubVideoManager;

		public FMain()
		{
			InitializeComponent();
			MyGuiComponents.InitializeAndFormatFormComponents(this);
		}

		private void FMain_Load(object sender, EventArgs e)
		{
			if (!Directory.Exists(Paths.DataFolder))
				Directory.CreateDirectory(Paths.DataFolder);
			if (!Directory.Exists(Paths.ImageFolder))
				Directory.CreateDirectory(Paths.ImageFolder);
			//
			try
			{
				SettingsForm = new FSettings(this);
				SubEditorForm = new FSubEditor(this);
				VideoForm = new FVideo(this);
			}
			catch (Exception E)
			{
				MessageBox.Show("An error occured while initializing other forms (in FMain_Load).\n\n" + E.ToString(), "Initialization ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Application.Exit();
			}
			//
			MenuP.SetBounds(ControlPadding, ControlPadding, this.Width - 2 * ControlPadding, 60);
			StatusP.SetBounds(MenuP.Left, MenuP.Bottom + ControlPadding, MenuP.Width, 32);
			SubListP.SetBounds(ControlPadding, StatusP.Bottom + ControlPadding, 320, this.Height - MenuP.Height - StatusP.Height - 4 * ControlPadding);
			VideoListP.SetBounds(SubListP.Right + ControlPadding, SubListP.Top, this.Width - SubListP.Right - 2 * ControlPadding, SubListP.Height);
			MenuButtons = MyGuiComponents.CreateMenuButtons(MenuP, MenuButtonCaptions, true, MenuButton_Click);
			StatusBar = new PictureBoxStatusBar(StatusP, "Feel free to browse, update or edit subscriptions, or to play around with the settings!", 100);
		}

		private void FMain_Shown(object sender, EventArgs e)
		{
			string loadResult = TDatabase.OpenDatabase(out Database);
			if (!loadResult.Equals(""))
			{
				MessageBox.Show(loadResult, "Database open ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Application.Exit();
			}
			else
			{
				Settings = new TSettings();
				Settings.ReadFromFile();
				SubManager = new SubscriptionManager(Database, SubListP, SubscriptionBox_Click);
				SubVideoManager = new SubscriptionVideoManager(SubManager.SelectedSubBox.Youtuber, VideoListP, VideoBox_Click);
			}
		}

		private void MenuButton_Click(object sender, EventArgs e)
		{
			int r = MenuButtonCaptions.IndexOf(((PictureBoxButton) sender).Information);
			switch (r)
			{
				case 0: // update all
					if (SubManager.GetThreadsWithStatus(SubscriptionBox.StatusUpdating) > 0)
					{
						MessageBox.Show("Some threads are still working! Please wait until they finish to exit.", "Have patience", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					StatusBar.UpdateStatus("Preparing for update...", 0);
					SubManager.UpdateAllSubscriptions();
					UpdateAllTimer.Enabled = true;
					break;
				case 1: // update current
					if (SubManager.SelectedSubBox.CurrentStatus == SubscriptionBox.StatusUpdating)
						return;
					StatusBar.UpdateStatus("Preparing for update...", 0);
					SubManager.SelectedSubBox.UpdateThread.RunWorkerAsync();
					UpdateCurrentTimer.Enabled = true;
					break;
				case 2: // edit subs
					SubEditorForm.Show();
					break;
				case 3: // settings/about 
					SettingsForm.Show();
					break;
				case 4: // exit					
					if (SubManager.GetThreadsWithStatus(SubscriptionBox.StatusUpdating) > 0)
					{
						MessageBox.Show("Some threads are still working! Please wait until they finish to exit.", "Have patience", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					SettingsForm.Dispose();
					SubEditorForm.Dispose();
					VideoForm.Dispose();
					Application.Exit();
					break;
			}
		}

		private void UpdateTimer_Tick(object sender, EventArgs e)
		{
			int nUpd = SubManager.GetThreadsWithStatus(SubscriptionBox.StatusUpdating), total = SubManager.Database.Youtubers.Count;
			if (nUpd > 0)
				StatusBar.UpdateStatus(string.Format("Updating subscriptions ({0}/{1} completed)...", total - nUpd, total), ((total - nUpd) * 100) / total);
			else
			{
				UpdateAllTimer.Enabled = false;
				if (SubManager.GetThreadsWithStatus(SubscriptionBox.StatusFailed) == 0)
					StatusBar.UpdateStatus(string.Format("All {0} subscriptions have been updated!", total), 100);
				else
					StatusBar.UpdateStatus(string.Format("Only {0} of {1} subscriptions have been updated.", SubManager.GetThreadsWithStatus(SubscriptionBox.StatusReady), total), 100);
				SubscriptionBox_Click(SubManager.SelectedSubBox, null);
				SubscriptionBox.DrawSubscriptionBox(SubManager.SelectedSubBox, false, null);
				//
				string saveResult = TDatabase.SaveDatabase(Database);
				if (!saveResult.Equals(""))
					MessageBox.Show(saveResult, "Database save ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void UpdateCurrentTimer_Tick(object sender, EventArgs e)
		{
			if (SubManager.SelectedSubBox.CurrentStatus == SubscriptionBox.StatusUpdating)
				StatusBar.UpdateStatus("Updating subscription to " + SubManager.SelectedSubBox.Youtuber.Name + "...", 0);
			else
			{
				UpdateCurrentTimer.Enabled = false;
				if (SubManager.SelectedSubBox.CurrentStatus == SubscriptionBox.StatusFailed)
					MessageBox.Show("Failed to update subscription to " + SubManager.SelectedSubBox.Youtuber.Name + " :( .", "Update ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				/*if (SubManager.SelectedSubBox.CurrentStatus == SubscriptionBox.StatusReady)
					StatusBar.UpdateStatus("Successfully updated subscription to " + SubManager.SelectedSubBox.Youtuber.Name + " !", 100);
				else
					StatusBar.UpdateStatus("Failed to update subscription to " + SubManager.SelectedSubBox.Youtuber.Name + " :( .", 100);*/
				string saveResult = TDatabase.SaveDatabase(Database);
				if (!saveResult.Equals(""))
					MessageBox.Show(saveResult, "Database save ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void SubscriptionBox_Click(object sender, EventArgs e)
		{
			if (e == null || ((MouseEventArgs) e).Button == MouseButtons.Left)
			{
				if (SubManager.GetThreadsWithStatus(SubscriptionBox.StatusUpdating) > 0)
					return;
				SubscriptionBox clickedSB = sender as SubscriptionBox;
				SubManager.SelectedSubBox = clickedSB;
				foreach (SubscriptionBox sb in SubManager.SubscriptionBoxes)
					if (sb.Visible)
					{
						sb.Checked = sb.Youtuber.ID.Equals(clickedSB.Youtuber.ID);
						SubscriptionBox.DrawSubscriptionBox(sb, sb.Checked, null);
					}
				SubVideoManager.Youtuber = clickedSB.Youtuber;
				if (SubVideoManager.Youtuber.Videos.Count > 0)
					StatusBar.UpdateStatus(string.Format("[{0}]: info last updated {1} ({2} starting {3})",
						SubVideoManager.Youtuber.Name,
						Utils.FormatElapsedTime(DateTime.Now.Subtract(SubVideoManager.Youtuber.InfoLastUpdated)),
						Utils.RegularPlural("video", SubVideoManager.Youtuber.Videos.Count, true),
						Utils.FormatDateTime(SubVideoManager.Youtuber.Videos[0].Uploaded, "d MMMM yyyy 'at' HH:mm")), 100);
				else
					StatusBar.UpdateStatus(string.Format("[{0}]: info last updated {1} (no videos)",
						SubVideoManager.Youtuber.Name,
						Utils.FormatElapsedTime(DateTime.Now.Subtract(SubVideoManager.Youtuber.InfoLastUpdated))), 100);
				SubVideoManager.RecreateVideoBoxes();
			}
			else
				MessageBox.Show("something about comparisons");
		}

		private void VideoBox_Click(object sender, EventArgs e)
		{
			if (((MouseEventArgs) e).Button == MouseButtons.Left)
			{
				if (SubManager.GetThreadsWithStatus(SubscriptionBox.StatusUpdating) > 0)
					return;
				VideoForm.Show();
				VideoForm.RefreshInfo(SubManager.SelectedSubBox.Youtuber, (sender as VideoBox).Video);
			}
			else
				MessageBox.Show("something about something else");
		}
	}
}
