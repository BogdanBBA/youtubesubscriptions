﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YoutubeSubscriptions.Classes;

namespace YoutubeSubscriptions.Forms
{
	public partial class FSettings : Form
	{
		private static List<string> MenuButtonCaptions = new List<string>(new string[1] { "Save and close" });

		private List<PictureBoxButton> MenuButtons;
		private FMain MainForm;

		public FSettings(FMain MainForm)
		{
			InitializeComponent();
			this.MainForm = MainForm;
			MyGuiComponents.InitializeAndFormatFormComponents(this);
		}

		private void FSettings_Load(object sender, EventArgs e)
		{
			MenuButtons = MyGuiComponents.CreateMenuButtons(MenuP, MenuButtonCaptions, true, MenuButton_Click);
			foreach (string fmt in TSettings.UploadedFormats)
				dateFormat.Items.Add(Utils.FormatDateTime(DateTime.Now, fmt));
			RefreshSettings();
		}

		private void RefreshSettings()
		{
			TSettings sett = MainForm.Settings;
			daysNUD.Value = sett.VideoDaysGoBack;
			videoColsNUD.Value = sett.VideoColumns;
			dateFormat.SelectedIndex = sett.UploadedFormatIndex;
			showVideoTitleChB.Checked = sett.ShowVideoTitle;
			showVideoStatsChB.Checked = sett.ShowVideoStats;
			CheckBox_CheckedChanged(showVideoTitleChB, null);
			CheckBox_CheckedChanged(showVideoStatsChB, null);
		}

		private void MenuButton_Click(object sender, EventArgs e)
		{
			int r = MenuButtonCaptions.IndexOf(((PictureBoxButton) sender).Information);
			switch (r)
			{
				case 0: // save and close
					// get settings
					TSettings sett = MainForm.Settings;
					sett.VideoDaysGoBack = (int) daysNUD.Value;
					sett.VideoColumns = (int) videoColsNUD.Value;
					sett.UploadedFormatIndex = dateFormat.SelectedIndex;
					sett.ShowVideoTitle = showVideoTitleChB.Checked;
					sett.ShowVideoStats = showVideoStatsChB.Checked;
					// actual save and close
					this.Hide();
					sett.SaveToFile();
					MainForm.SubVideoManager.RecreateVideoBoxes();
					MainForm.Focus();
					break;
				default: // something else
					MessageBox.Show("Not implemented yet, WTF?!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					break;
			}
		}

		private void daysNUD_ValueChanged(object sender, EventArgs e)
		{
			if (daysNUD.Value > daysNUD.Maximum)
				daysNUD.Value = daysNUD.Maximum;
		}

		private void CheckBox_CheckedChanged(object sender, EventArgs e)
		{
			(sender as CheckBox).Text = (sender as CheckBox).Checked ? "Yes" : "No";
		}
	}
}
