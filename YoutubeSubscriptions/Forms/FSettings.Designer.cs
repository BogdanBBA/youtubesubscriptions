﻿namespace YoutubeSubscriptions.Forms
{
	partial class FSettings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.daysNUD = new System.Windows.Forms.NumericUpDown();
			this.MenuP = new System.Windows.Forms.Panel();
			this.videoColsNUD = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.dateFormat = new System.Windows.Forms.ComboBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.showVideoTitleChB = new System.Windows.Forms.CheckBox();
			this.showVideoStatsChB = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.daysNUD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.videoColsNUD)).BeginInit();
			this.SuspendLayout();
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(22, 126);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(449, 21);
			this.label3.TabIndex = 8;
			this.label3.Text = "From the last how many days ago to include videos (on update)";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.Orange;
			this.label2.Location = new System.Drawing.Point(22, 102);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(163, 21);
			this.label2.TabIndex = 7;
			this.label2.Text = "Application settings";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(22, 43);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(291, 21);
			this.label1.TabIndex = 11;
			this.label1.Text = "created by BogdanBBA @ Wussa\'s home";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.Color.Orange;
			this.label4.Location = new System.Drawing.Point(22, 19);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(248, 21);
			this.label4.TabIndex = 10;
			this.label4.Text = "About \"Youtube Subscriptions\"";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.Color.White;
			this.label5.Location = new System.Drawing.Point(22, 64);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(259, 21);
			this.label5.TabIndex = 12;
			this.label5.Text = "v1.0 - August 16th, 20th, 25th; 2014";
			// 
			// daysNUD
			// 
			this.daysNUD.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.daysNUD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.daysNUD.Location = new System.Drawing.Point(26, 150);
			this.daysNUD.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
			this.daysNUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.daysNUD.Name = "daysNUD";
			this.daysNUD.Size = new System.Drawing.Size(120, 25);
			this.daysNUD.TabIndex = 13;
			this.daysNUD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.daysNUD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.daysNUD.ValueChanged += new System.EventHandler(this.daysNUD_ValueChanged);
			// 
			// MenuP
			// 
			this.MenuP.Location = new System.Drawing.Point(99, 323);
			this.MenuP.Name = "MenuP";
			this.MenuP.Size = new System.Drawing.Size(300, 45);
			this.MenuP.TabIndex = 14;
			// 
			// videoColsNUD
			// 
			this.videoColsNUD.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.videoColsNUD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.videoColsNUD.Location = new System.Drawing.Point(26, 211);
			this.videoColsNUD.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
			this.videoColsNUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.videoColsNUD.Name = "videoColsNUD";
			this.videoColsNUD.Size = new System.Drawing.Size(120, 25);
			this.videoColsNUD.TabIndex = 16;
			this.videoColsNUD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.videoColsNUD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(22, 187);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(113, 21);
			this.label6.TabIndex = 15;
			this.label6.Text = "Video columns";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(212, 183);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(187, 21);
			this.label7.TabIndex = 17;
			this.label7.Text = "Video upload date format";
			// 
			// dateFormat
			// 
			this.dateFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dateFormat.FormattingEnabled = true;
			this.dateFormat.Location = new System.Drawing.Point(216, 207);
			this.dateFormat.MaxDropDownItems = 20;
			this.dateFormat.Name = "dateFormat";
			this.dateFormat.Size = new System.Drawing.Size(229, 29);
			this.dateFormat.TabIndex = 18;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(212, 249);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(250, 21);
			this.label8.TabIndex = 21;
			this.label8.Text = "Show video upload date and views";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(22, 249);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(121, 21);
			this.label9.TabIndex = 19;
			this.label9.Text = "Show video title";
			// 
			// showVideoTitleChB
			// 
			this.showVideoTitleChB.Location = new System.Drawing.Point(26, 273);
			this.showVideoTitleChB.Name = "showVideoTitleChB";
			this.showVideoTitleChB.Size = new System.Drawing.Size(90, 24);
			this.showVideoTitleChB.TabIndex = 22;
			this.showVideoTitleChB.Text = "checkBox1";
			this.showVideoTitleChB.UseVisualStyleBackColor = true;
			this.showVideoTitleChB.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
			// 
			// showVideoStatsChB
			// 
			this.showVideoStatsChB.Location = new System.Drawing.Point(216, 273);
			this.showVideoStatsChB.Name = "showVideoStatsChB";
			this.showVideoStatsChB.Size = new System.Drawing.Size(90, 24);
			this.showVideoStatsChB.TabIndex = 23;
			this.showVideoStatsChB.Text = "checkBox2";
			this.showVideoStatsChB.UseVisualStyleBackColor = true;
			this.showVideoStatsChB.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
			// 
			// FSettings
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlDark;
			this.ClientSize = new System.Drawing.Size(489, 409);
			this.ControlBox = false;
			this.Controls.Add(this.showVideoStatsChB);
			this.Controls.Add(this.showVideoTitleChB);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.dateFormat);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.videoColsNUD);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.MenuP);
			this.Controls.Add(this.daysNUD);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "FSettings";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Settings";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.FSettings_Load);
			((System.ComponentModel.ISupportInitialize)(this.daysNUD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.videoColsNUD)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown daysNUD;
		private System.Windows.Forms.Panel MenuP;
		private System.Windows.Forms.NumericUpDown videoColsNUD;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox dateFormat;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.CheckBox showVideoTitleChB;
		private System.Windows.Forms.CheckBox showVideoStatsChB;
	}
}