﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YoutubeSubscriptions.Classes;

namespace YoutubeSubscriptions
{
	public partial class FSubEditor : Form
	{
		private static List<string> EditButtonCaptions = new List<string>(new string[3] { "Add new subscription", "Delete currently selected", "Save currently selected" });
		private static List<string> FormControlButtonCaptions = new List<string>(new string[1] { "CLOSE EDITOR" });

		private List<PictureBoxButton> EditButtons, FormControlButtons;
		private FMain MainForm;

		//private 

		public FSubEditor(FMain MainForm)
		{
			InitializeComponent();
			this.MainForm = MainForm;
			MyGuiComponents.InitializeAndFormatFormComponents(this);
		}

		private void FSubEditor_Load(object sender, EventArgs e)
		{
			EditButtons = MyGuiComponents.CreateMenuButtons(EditingP, EditButtonCaptions, false, EditButton_Click);
			FormControlButtons = MyGuiComponents.CreateMenuButtons(FormControlP, FormControlButtonCaptions, true, FormControlButton_Click);
			RefreshSubs();
		}

		private void RefreshSubs()
		{
			SubsTB.Items.Clear();
			foreach (TYoutuber y in MainForm.Database.Youtubers)
				SubsTB.Items.Add(y.ID);
			SubsTB.SelectedIndex = SubsTB.Items.Count > 0 ? 0 : -1;
		}

		private void SubsTB_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (SubsTB.SelectedIndex == -1)
			{
				YoutuberIDTB.Text = "";
				YoutuberNameTB.Text = "";
			}
			else
			{
				TYoutuber youtuber = MainForm.Database.GetYoutuberByID((string) SubsTB.Items[SubsTB.SelectedIndex]);
				if (youtuber == null)
				{
					MessageBox.Show("Could not find the youtuber with the ID \"" + (string) SubsTB.Items[SubsTB.SelectedIndex] + "\" !", "Save ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
					RefreshSubs();
					return;
				}
				YoutuberIDTB.Text = youtuber.ID;
				YoutuberNameTB.Text = youtuber.Name;
			}
		}

		private void EditButton_Click(object sender, EventArgs e)
		{
			int r = EditButtonCaptions.IndexOf(((PictureBoxButton) sender).Information);
			switch (r)
			{
				case 0: // add new
					TYoutuber youtuber = new TYoutuber();
					youtuber.ID = "newYoutuberID";
					youtuber.Name = "[new Youtuber]";
					youtuber.InfoLastUpdated = DateTime.Now;
					youtuber.Joined = new DateTime(2000, 1, 1);
					youtuber.Subscribers = 0;
					youtuber.TotalVideos = 0;
					youtuber.TotalViews = 0;
					youtuber.Videos = new List<TVideo>();
					MainForm.Database.Youtubers.Add(youtuber);
					RefreshSubs();
					SubsTB.SelectedIndex = SubsTB.Items.IndexOf(youtuber.ID);
					YoutuberIDTB.Focus();
					break;
				case 1: // delete current
					if (MainForm.Database.Youtubers.Count == 1)
					{
						MessageBox.Show("Please don't delete the last subscription. Edit it.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						break;
					}
					int selIndex = SubsTB.SelectedIndex;
					youtuber = MainForm.Database.GetYoutuberByID((string) SubsTB.Items[selIndex]);
					if (MessageBox.Show("Are you sure you want to delete the subscription to \"" + youtuber.ID + "\" ?", "Confirm delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						if (!MainForm.Database.Youtubers.Remove(youtuber))
							MessageBox.Show("Could not delete the current subscription :( !", "Delete ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
					RefreshSubs();
					SubsTB.SelectedIndex = selIndex < SubsTB.Items.Count ? selIndex : SubsTB.Items.Count - 1;
					break;
				case 2: // save current
					youtuber = MainForm.Database.GetYoutuberByID((string) SubsTB.Items[SubsTB.SelectedIndex]);
					youtuber.ID = YoutuberIDTB.Text;
					youtuber.Name = YoutuberNameTB.Text;
					SubsTB.Items[SubsTB.SelectedIndex] = youtuber.ID;
					MessageBox.Show("Successfully saved subscription!\n\nTo memory, that is. The database will only be saved to file when closing the editor.", "Subscription saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
					break;
				case 3: // something else
					MessageBox.Show("Not implemented yet, WTF?!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					break;
			}
		}

		private void FormControlButton_Click(object sender, EventArgs e)
		{
			int r = FormControlButtonCaptions.IndexOf(((PictureBoxButton) sender).Information);
			switch (r)
			{
				case 0:
					string loadResult = TDatabase.SaveDatabase(MainForm.Database);
					if (!loadResult.Equals(""))
						MessageBox.Show(loadResult, "Database save ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
					this.Hide();
					MainForm.SubManager.RecreateSubscriptionBoxes();
					MainForm.Focus();
					break;
				case 1: // something else
					MessageBox.Show("Not implemented yet, WTF?!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					break;
			}
		}
	}
}
